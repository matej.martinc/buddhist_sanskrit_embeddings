import torch
from transformers import AutoModelForMaskedLM, AutoModelForCausalLM
import numpy as np
import dill
import gc
import pandas as pd
import argparse
from sklearn.metrics.pairwise import cosine_similarity

from transformers import PreTrainedTokenizerFast

BOS_TOKEN='[CLS]'
EOS_TOKEN='[SEP]'

def get_shifts(input_path):
    shifts_dict = {}
    df_shifts = pd.read_csv(input_path, sep=',', encoding='utf8')
    for idx, row in df_shifts.iterrows():
        shifts_dict[row['word']] = row['mean']
    return shifts_dict


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]


def add_embedding_to_list(previous, word_emb):
    embeds = [x[0] / x[1] for x in previous]
    cs = list(cosine_similarity(word_emb.reshape(1, -1), np.array(embeds))[0].tolist())
    if len(previous) < 1:
        max_idx = len(previous)
        previous.append((word_emb, 1))
    else:
        max_idx = cs.index(max(cs))
        old_embd, count = previous[max_idx]
        new_embd = old_embd + word_emb
        count = count + 1
        previous[max_idx] = (new_embd, count)
    return previous, max_idx


def addPosition(sent_data, length):
    mapping, sent, target = sent_data
    new_mapping = []
    for word, idxs in mapping:
        new_idxs = [i + length for i in idxs]
        new_mapping.append((word, new_idxs))
    return (new_mapping, sent, target)


def mapSent2target(sent, target, original_sent):
    conjunctives = ['-']
    target = target.split()
    mapping = []
    count = 0
    idxs = []
    all_idxs = []
    for idx, token in enumerate(sent):
        if token in [EOS_TOKEN, BOS_TOKEN]:
            continue
        elif sent[idx + 1] in conjunctives or sent[idx + 1].startswith('##'):
            idxs.append(idx)
        elif token in conjunctives:
            idxs.append(idx)
        elif sent[idx + 1] not in conjunctives and not sent[idx + 1].startswith('##'):
            idxs.append(idx)
            all_idxs.extend(idxs)
            try:
                mapping.append((target[count], idxs))
            except:
                print('Error in mapping')
                print(sent)
                print(target)
                print('----------------------')
                return None

            count += 1
            idxs = []
    length_sent = len(sent)
    length_mapping = len(all_idxs)
    if length_sent - 2 != length_mapping:
        print('Wrong length')
        print(sent)
        print(target)
        print('----------------------')
        return None
    return (mapping, original_sent, target)


def tokens_to_batches(ds, tokenizer, batch_size, max_length):
    batches = []
    batch = []
    batch_counter = 0

    counter = 0
    sent_counter = 0
    sent2count = {}
    count2sent = {}
    all_vocab = set()
    prev_vocab = 0
    error_counter = 0

    for doc, target_doc, meta in ds:
        sents = doc.split(' <eos> ')
        target_sents = target_doc.split(' <eos> ')
        tokenized_text = []
        seq_mappings = []

        for sent, target_sent in zip(sents, target_sents):

            sent = sent.replace('&amp;', '&')
            sent = sent.replace('&lt;', 'lt')
            sent = sent.replace('&gt;', 'gt')
            counter += 1

            if counter % 500000 == 0:
                print('Num sentences: ', counter)
            if len(all_vocab) % 100000 == 0 and len(all_vocab) > 0 and len(all_vocab) > prev_vocab:
                prev_vocab = len(all_vocab)
                print('Vocab size: ', len(all_vocab))

            sent_counter += 1
            lsent = sent.strip()
            if len(lsent.split()) > 2:
                marked_sent = BOS_TOKEN + " " + lsent + " " + EOS_TOKEN
                tokenized_sent = tokenizer.tokenize(marked_sent)
                if len(tokenized_sent) > max_length:
                    continue
                tokenized_target = tokenizer.tokenize(target_sent)
                target_sent = tokenizer.convert_tokens_to_string(tokenized_target)

                mapping = mapSent2target(tokenized_sent, target_sent, sent)
                if mapping is None:
                    error_counter += 1
                    continue
                targets = mapping[2]

                for l in targets:
                    all_vocab.add(l)

                count2sent[sent_counter] = (sent, target_sent, meta)
                sent2count[sent] = sent_counter

                if len(tokenized_text) + len(tokenized_sent) > max_length:
                    indexed_tokens = tokenizer.convert_tokens_to_ids(tokenized_text)
                    batch.append((indexed_tokens, seq_mappings, tokenized_text))
                    batch_counter += 1
                    tokenized_text = tokenized_sent
                    seq_mappings = [mapping]
                    if batch_counter % batch_size == 0:
                        batches.append(batch)
                        batch = []
                else:
                    mapping = addPosition(mapping, len(tokenized_text))
                    seq_mappings.append(mapping)
                    tokenized_text.extend(tokenized_sent)

        if len(tokenized_text) > 0:
            indexed_tokens = tokenizer.convert_tokens_to_ids(tokenized_text)
            batch.append((indexed_tokens, seq_mappings, tokenized_text))
            batch_counter += 1
            if batch_counter % batch_size == 0:
                batches.append(batch)
                batch = []

    if batch_counter % batch_size != 0:
        batches.append(batch)

    print()
    print('Tokenization done!')
    print("Num. wrong mappings:", error_counter)
    print('Final Vocab size: ', len(all_vocab))
    print('len batches: ', len(batches))

    return batches, count2sent, sent2count


def get_token_embeddings(batches, model, batch_size, layers, device):

    encoder_token_embeddings = []
    mappings = []
    tokens = []
    counter = 0

    for batch in batches:
        counter += 1
        if counter % 1000 == 0:
            print('Generating embedding for batch: ', counter)
        lens = [len(x[0]) for x in batch]
        max_len = max(lens)
        tokens_tensor = torch.zeros(batch_size, max_len, dtype=torch.long).to(device)
        segments_tensors = torch.ones(batch_size, max_len, dtype=torch.long).to(device)
        batch_idx = [x[0] for x in batch]
        batch_mapping = [x[1] for x in batch]
        batch_text = [x[2] for x in batch]

        for i in range(len(batch)):
            length = len(batch_idx[i])
            for j in range(max_len):
                if j < length:
                    tokens_tensor[i][j] = batch_idx[i][j]

        # Predict hidden states features for each layer
        with torch.no_grad():
            model_output = model(tokens_tensor, token_type_ids=segments_tensors)
            if layers == 'last_4':
                encoded_layers = model_output['hidden_states'][-4:]
            elif layers == 'first_6':
                encoded_layers = model_output['hidden_states'][1:7]
            elif layers == 'all':
                encoded_layers = model_output['hidden_states'][1:]

        for batch_i in range(len(batch)):
            encoder_token_embeddings_example = []
            # For each token in the sentence...
            for token_i in range(len(batch_idx[batch_i])):

                # Holds 12 layers of hidden states for each token
                hidden_layers = []
                #print(len(encoded_layers))

                # For each of the 12 layers...
                for layer_i in range(len(encoded_layers)):
                    # Lookup the vector for `token_i` in `layer_i`
                    vec = encoded_layers[layer_i][batch_i][token_i]
                    hidden_layers.append(vec)

                hidden_layers = torch.sum(torch.stack(hidden_layers), 0).reshape(1, -1).detach().cpu().numpy()

                encoder_token_embeddings_example.append(hidden_layers)

            encoder_token_embeddings.append(encoder_token_embeddings_example)
            mappings.append(batch_mapping[batch_i])
            tokens.append(batch_text[batch_i])

        # Sanity check the dimensions:
        #print("Number of tokens in sequence:", len(token_embeddings))
        #print("Number of layers per token:", len(token_embeddings[0]))

    return encoder_token_embeddings, mappings, tokens


def combine_bpe(idxs, context_embedding, tokens):
    if len(idxs) == 1:
        encoder_array = context_embedding[idxs[0]]
    else:
        lengths = [len(tokens[i].replace('##', '')) for i in idxs]
        weights = [x/sum(lengths) for x in lengths]

        encoder_array = np.zeros((1, 768))
        for i, weight in zip(idxs, weights):
            encoder_array += (context_embedding[i] * weight)
    return encoder_array


def get_embeddings(embeddings_path, vocab, tokenizer, model, batch_size, max_length, layers, device):
    vocab_vectors = {}
    count2sents = {}
    not_in_targets = set()
    targets = set(x[0] for x in vocab.freqs)

    for chunk in vocab.chunks:
        print("WORKING ON CORPUS: ", chunk)
        ds = zip(vocab.docs[chunk], vocab.target_docs[chunk], vocab.meta[chunk])
        all_batches,  count2sent, sent2count = tokens_to_batches(ds, tokenizer, batch_size, max_length)

        count2sents[chunk] = count2sent
        chunked_batches = chunks(all_batches, 1000)
        num_chunk = 0

        for batches in chunked_batches:
            num_chunk += 1
            print('Chunk ', num_chunk)

            #get list of embeddings and list of bpe tokens
            encoder_token_embeddings, mappings, tokens = get_token_embeddings(batches, model, batch_size, layers, device)

            #go through text token by token
            for emb_idx, context_embedding in enumerate(encoder_token_embeddings):
                seq_mappings = mappings[emb_idx]
                for mapping, sentence, target_sent in seq_mappings:
                    sent_tokens = []
                    for token_i, idxs in mapping:
                        if token_i in targets:
                            encoder_array = combine_bpe(idxs, context_embedding, tokens[emb_idx])

                            if token_i in vocab_vectors:
                                if chunk in vocab_vectors[token_i]:
                                    previous = vocab_vectors[token_i][chunk]
                                    new, new_idx = add_embedding_to_list(previous, encoder_array.squeeze())
                                    vocab_vectors[token_i][chunk] = new
                                    sent_tokens.append((token_i, new_idx))
                                else:
                                    vocab_vectors[token_i][chunk] = [(encoder_array.squeeze(), 1)]
                                    vocab_vectors[token_i][chunk + '_text'] = {}
                                    sent_tokens.append((token_i, 0))
                            else:
                                vocab_vectors[token_i] = {chunk: [(encoder_array.squeeze(), 1)], chunk + '_text': {}}
                                sent_tokens.append((token_i, 0))
                        else:
                            not_in_targets.add(token_i)

                    for sent_token, sent_idx in sent_tokens:
                        if sent_idx in vocab_vectors[sent_token][chunk + '_text']:
                            vocab_vectors[sent_token][chunk + '_text'][sent_idx].append(sent2count[sentence])
                        else:
                            vocab_vectors[sent_token][chunk + '_text'][sent_idx] = [sent2count[sentence]]

            del encoder_token_embeddings
            del batches
            gc.collect()

        print('Sentence embeddings generated.')
        print('Tokens not in targets: ', len(not_in_targets))

    print("Length of vocab after training: ", len(vocab_vectors.items()))

    with open(embeddings_path.split('.')[0] + '.pickle', 'wb') as handle:
        dill.dump([vocab_vectors, count2sents], handle)

    gc.collect()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--vocab_path",
                        default='resources/vocab.pickle',
                        type=str,
                        help="Paths to vocab pickle file generated by the preprocessing.py script")
    parser.add_argument("--embeddings_path",
                        default='embeddings/embeddings.pickle', type=str,
                        help="Path to output pickle file containing embeddings.")
    parser.add_argument("--model_type",
                        default='BERT',
                        type=str,
                        help="BERT or GPT-2")
    parser.add_argument("--layer_config",
                        default='all',
                        type=str,
                        help="Which encoder layers to take to generate embedding, options are 'all', 'last_4', 'first_6'")
    parser.add_argument("--path_to_fine_tuned_model",
                        default='models_reference_corpus_fine_tuned',
                        type=str,
                        help="Path to fine-tuned model. If empty, pretrained model is used")
    parser.add_argument("--batch_size", default=8, type=int, help="Batch size.")
    parser.add_argument("--max_sequence_length", default=128, type=int)
    parser.add_argument("--tokenizer",
                        default='tokenizers/sanskrit.json',
                        type=str,
                        help="Path to the trained tokenizer")
    parser.add_argument("--device",
                        default='cuda',
                        type=str,
                        help="Which gpu to use")
    args = parser.parse_args()

    batch_size = args.batch_size
    max_length = args.max_sequence_length

    assert args.layer_config in ['all', 'last_4', 'first_6']
    assert args.model_type in ['BERT', 'GPT-2']

    layer = args.layer_config
    tokenizer_path = args.tokenizer
    embeddings = args.embeddings_path
    device = torch.device(args.device if torch.cuda.is_available() else "cpu")

    embeddings_path = args.embeddings_path
    print('Generating embeddings for the following config:')
    print('Vocab:', args.vocab_path)
    print('Model:', args.path_to_fine_tuned_model)
    print('Tokenizer:', tokenizer_path)
    print('Output embeddings path:', embeddings_path)
    with open(args.vocab_path, 'rb') as handle:
        vocab = dill.load(handle)
    tokenizer = PreTrainedTokenizerFast(tokenizer_file=tokenizer_path)
    if args.model_type == 'BERT':
        model = AutoModelForMaskedLM.from_pretrained(args.path_to_fine_tuned_model, output_hidden_states=True)
    else:
        model = AutoModelForCausalLM.from_pretrained(args.path_to_fine_tuned_model, output_hidden_states=True)
    model.to(device)
    model.eval()
    get_embeddings(embeddings_path, vocab, tokenizer, model, batch_size, max_length, layer, device)







