## Analogy dataset ##

Size: 24 sets of 5 morphologically related words<br>
Language: Buddhist Sanskrit<br>
Modality: written<br>
Use: freely available<br>
License: CREATIVE COMMONS CC BY SA<br>

## Simlex dataset ##

Size: 98 pairs of words<br>
Language: Buddhist Sanskrit<br>
Modality: written<br>
Use: freely available<br>
License: CREATIVE COMMONS CC BY SA<br>
