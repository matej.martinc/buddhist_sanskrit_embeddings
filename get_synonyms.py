import fasttext
import argparse
import dill
import numpy as np
import editdistance
import pandas as pd
import os
from unidecode import unidecode

def check_editdistance(target_word_list, compare_word, treshold=0.7):
    farEnough = True
    for w in target_word_list:
        ed = 1 - (float(editdistance.eval(unidecode(w), unidecode(compare_word))) / max(len(w), len(compare_word)))
        if ed > treshold:
            farEnough = False
            break
    return farEnough

def load_bert(emb_path, lang):
    word2vec = {}
    bert_embeddings, _ = dill.load(open(emb_path, 'rb'))
    for i, word in enumerate(bert_embeddings.keys()):
        vect = bert_embeddings[word][lang][0][0]/bert_embeddings[word][lang][0][1]
        if word in word2vec:
            print(word, 'found twice')
            continue
        word2vec[word] = vect
    return word2vec

def load_fasttext(emb_path):
    word2vec = {}
    model = fasttext.load_model(emb_path)
    vocab = model.words
    for w in vocab:
        word2vec[w] = model[w]
    return word2vec


def get_closest(word, word_emb, all_words, all_word_emb, K):
    scores = (all_word_emb / np.linalg.norm(all_word_emb, 2, 1)[:, None]).dot(word_emb / np.linalg.norm(word_emb))
    k_best = scores.argsort()[-K * 20:][::-1]
    best = []
    for i, idx in enumerate(k_best):
        close = all_words[idx]
        if unidecode(close) != unidecode(word) and unidecode(word) not in unidecode(close) and \
                unidecode(close) not in unidecode(word) and unidecode(close) not in best \
                and close.isalnum() and not any(char.isdigit() for char in close):
            best.append((close, str(round(scores[idx], 2))))
    best = [x[0] for x in best]
    filtered_best = [word]
    for w in best:
        if check_editdistance(filtered_best, w):
            filtered_best.append(w)
    return filtered_best[1:K+1]


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--embeddings_path",
                        default='embeddings/bert_buddhist_all_tokens_300_epochs_all_layers.pickle',
                        type=str,
                        help='Path to the embeddings model')
    parser.add_argument("--embeddings_type", default='bert',
                        help="bert, bert_small or fasttext")
    parser.add_argument("--results_path",
                        default='results', type=str,
                        help="Path to folder that will contain a file with results.")
    parser.add_argument("--words", default='paryāya-śabda, vac, dāna, ghoṣa, ālaṃkāra, yuga, yathā, abhisaraṇa, '
                                           'caraṇa, abhiprāya, vastu, parikalpa, sṃrti, lakṣaṇa, paricchinna, '
                                           'dā, tarka, tark, yukta, parikalpita, nibhatā, vidhi, kṛ, '
                                           'paricchin, pada, vitarka, kāma, saṃjñā, akṣara, vicāra, '
                                           'nidānāṅga, deśanā, vacana, kalpita, kārya, karaṇa, pratijān, saṃskṛta, '
                                           'parikalp, pratijñāta, deśita, vikalpa, prajñapti, āvṛt, kalpanā, vyavahāra, '
                                           'hita, vyāpāra, goṣṭha, maśaka, vivṛta, saṃkḷpta, kṛta, eva, vacanīya, kar, '
                                           'nānātva, bhrānti, śabda, cār, upaskara, kalpataru, abhilāpa, anusmṛti, '
                                           'datta, vidhā, praticint, saṃnidhi, nāma, smṛti, citra, kathā, '
                                           'upalabdhi, pratijñā, saṃketa, tathātā, abhidheya, citta',
                        help="list of words for which synonyms should be returned separated by comma")
    parser.add_argument("--num_neigh", default=10, type=int, help="Number of neighbor words returned, default is 10")
    args = parser.parse_args()

    target_words = [w.strip() for w in args.words.split(',')]
    if args.embeddings_type == 'bert_small':
        static_model = load_bert(args.embeddings_path, 'buddhist')
    elif args.embeddings_type == 'bert':
        static_model = load_bert(args.embeddings_path, 'buddh')
    elif args.embeddings_type == 'fasttext':
        static_model = load_fasttext(args.embeddings_path)
    else:
        raise Exception('embedding type should be bert, bert_small or fasttext')

    all_words = [x[0] for x in static_model.items()]
    all_word_emb = np.array([x[1] for x in static_model.items()])
    missing_words = []
    results = []
    for w in sorted(target_words):
        if w in static_model:
            w_emb = static_model[w]
            w_neigh = get_closest(w, w_emb, all_words, all_word_emb, K=args.num_neigh)
            results.append((w, ", ".join(w_neigh)))
        else:
            missing_words.append(w)

    print('Words for which embeddings are missing: ', ", ".join(missing_words))
    df_neigh = pd.DataFrame(results, columns=['word', 'synonyms'])
    if not os.path.exists(args.results_path):
        os.makedirs(args.results_path)
    print('Results written to file', os.path.join(args.results_path, 'synonyms_' + args.embeddings_type +'.tsv'))
    df_neigh.to_csv(os.path.join(args.results_path, 'synonyms_' + args.embeddings_type +'.tsv'), sep='\t', index=False)


