import random
import numpy as np
import fasttext
import os
from static_embeddings_experiments.fasttext.analogy_evaluation import evaluate

import argparse

def random_search(data_file, model_path):
    random.seed(42)
    log = os.path.join(model_path, "log")
    parameters = {}

    #subset of the analogies dataset which was used for hyperparameter optimization
    analogies = [['abhilap', 'abhilāpa'],
                ['kalp', 'kalpa'],
                ['smar', 'smṛti'],
                ['sthā', 'sthiti'],
                ['vac', 'vāc'],
                ['deś', 'deśanā'],
                ['darś', 'dṛṣṭi'],
                ['kath', 'kathā'],
                ['abhiniviś', 'abhiniveśa'],
                ['sev', 'sevā'],
                ['vyavahar', 'vyavahāra'],
                ['vikalp', 'vikalpa'],
                ['saṃkalp', 'saṃkalpa'],
                ['saṃjan', 'saṃjñā'],
                ['prajñap', 'prajñapti'],
                ['spṛś', 'sparśa'],
                ['car', 'cāra'],
                ['vicār', 'vicāra'],
                ['vart', 'vṛtti'],
                ['avalok', 'avaloka'],
                ['vyavalok', 'vyavaloka'],
                ['labh', 'lābha'],
                ['gṛhṇ', 'graha'],
                ['śru', 'śruti']]

    for i in range(100):
        model_name = os.path.join(model_path, "model_" + str(i) + ".bin")
        parameters['model'] = random.choice(['cbow', 'skipgram'])
        parameters['dim'] = random.choice([50, 100, 150, 200, 250, 300])
        parameters['ws'] = random.choice(np.arange(5, 11))
        parameters['epoch'] = random.choice(np.arange(3, 16))
        parameters['minn'] = random.choice(np.arange(3, 6))
        parameters['maxn'] = random.choice(np.arange(6, 9))
        with open(log, 'a') as f:
            f.write("Model {}\n".format(str(i)))
            for k, v in parameters.items():
                f.write("{}: {}\n".format(k, str(v)))
        model = fasttext.train_unsupervised(data_file, model=parameters['model'], dim=parameters['dim'],
                                            ws=parameters['ws'], epoch=parameters['epoch'], minn=parameters['minn'],
                                            maxn=parameters['maxn'])
        model.save_model(model_name)
        at1, at5, at10 = evaluate(model_name, analogies)
        with open(log, 'a') as f:
            f.write("@1: {}, @5: {}, @10: {}\n".format(at1, at5, at10))
            f.write("\n")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("--data_path",
                        type=str,
                        required=True)
    
    args = parser.parse_args()
    random_search(args.data_path, "../hyperparameter_optimization/fasttext")
