import fasttext
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("--data_path",
                        type=str,
                        required=True)
    parser.add_argument("--model_path",
                        type=str,
                        default="./trained_fasttext_model")
    parser.add_argument("--vector_size",
                        type=int,
                        default=300)
    parser.add_argument("--window_size",
                        type=int,
                        default=5)
    parser.add_argument("--epochs",
                        type=int,
                        default=10)
    parser.add_argument("--model",
                        type=str,
                        default='cbow')
    parser.add_argument("--minn",
                        type=int,
                        default=5)
    parser.add_argument("--maxn",
                        type=int,
                        default=5)

    args = parser.parse_args()
    data_file = args.data_path
    model = fasttext.train_unsupervised(data_file, model=args.model, dim=args.vector_size,
                                            ws=args.windows_size, epoch=args.epochs, minn=args.minn,
                                            maxn=args.maxn)
    model.save_model(args.model_path)