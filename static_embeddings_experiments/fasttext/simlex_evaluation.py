import fasttext
import pandas as pd
import numpy as np
from scipy.stats import spearmanr
import os
import argparse

def simlex_evaluation(simlex_file, model_path, log_file):
    data = pd.read_csv(simlex_file)
    model = fasttext.load_model(model_path)

    similarities = []
    gold_similarities = []
    for index, row in data.iterrows():
        w1 = model.get_word_vector(row['word1'])
        w2 = model.get_word_vector(row['word2'])
        cos = np.dot(w1, w2) / (np.sqrt(np.dot(w1, w1)) * np.sqrt(np.dot(w2, w2)))
        similarities.append(cos)
        gold_similarities.append((row['ScoreA'] + row['ScoreB'] + row['ScoreC']) / 3)

    correlation = spearmanr(gold_similarities, similarities)
    with open(log_file, 'a') as f:
        f.write("Spearman correlation\n")
        f.write("Correlation: " + str(correlation[0]))
        f.write("p-value: " + str(correlation[1]))

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("--model_path",
                        type=str,
                        required=True)
    parser.add_argument("--evaluation_file",
                        type=str,
                        default="../../resources/AllPairsScored_v3_2021-10-20.csv")
    parser.add_argument("--log_file",
                        type=str,
                        default="./fasttext_simlex_log.txt")
    
    args = parser.parse_args()
    simlex_evaluation(args.evaluaiton_file, args.model_path, args.log_file)
