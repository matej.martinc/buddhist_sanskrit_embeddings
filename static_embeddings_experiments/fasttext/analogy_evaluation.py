import fasttext
import argparse

def evaluate(model_path, analogies):
    """Evaluation function for hyperparameter optimization of embeddings models on the analogy task.
    The function evaluates the intermediate model embeddings on a subset of analogy candidates and outputs accuracy scores @1, @5 and @10."""
    model = fasttext.load_model(model_path)

    at1 = 0
    at5 = 0
    at10 = 0
    count = 0

    for i, a in enumerate(analogies):
        if i+1 == len(analogies):
            break
        predictions = model.get_analogies(a[0], a[1], analogies[i+1][0])
        pred_at_1 = predictions[0][1]
        if pred_at_1 == analogies[i+1][1]:
            at1 += 1

        pred = predictions[:5]
        pred_at_5 = []
        for p in pred:
            pred_at_5.append(p[1])
        if analogies[i+1][1] in pred_at_5:
            at5 += 1

        pred_at_10 = []
        for p in predictions:
            pred_at_10.append(p[1])
        if analogies[i+1][1] in pred_at_10:
            at10 += 1
        count += 1

    at1 = at1 / count
    at5 = at5 / count
    at10 = at10 / count
    return at1, at5, at10


def evaluate_all_pairs(model_path, analogies):
    """Evaluation function for final evaluation of trained embeddings on the analogy task.
    The function evaluates the final embeddings on all possible pairs of analogy candidates and outputs accuracy scores @1, @5 and @10."""
    model = fasttext.load_model(model_path)

    at1 = 0
    at5 = 0
    at10 = 0
    count = 0

    for index, a in enumerate(analogies):
        for index_b, b in enumerate(analogies):
            if b[0] == a[0]:
                continue
            predictions = model.get_analogies(a[0], a[1], b[0])
            pred_at_1 = predictions[0][1]
            if pred_at_1 == b[1]:
                at1 += 1

            pred = predictions[:5]
            pred_at_5 = []
            for p in pred:
                pred_at_5.append(p[1])
            if b[1] in pred_at_5:
                at5 += 1

            pred_at_10 = []
            for p in predictions:
                pred_at_10.append(p[1])
            if b[1] in pred_at_10:
                at10 += 1
            count += 1

    at1 = at1 / count
    at5 = at5 / count
    at10 = at10 / count
    return at1, at5, at10, count


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("--model_path",
                        type=str,
                        required=True)
    parser.add_argument("--log_file",
                        type=str,
                        default="./fasttext_analogy_log.txt")

    args = parser.parse_args()
    
    columns = [[0,2], [0,1], [1,2]]
    column_names = ['verb', 'ppp', 'noun']
    analogies = [['kalp', 'kalpita', 'kalpa'],
                 ['smar', 'smṛta', 'smṛti'],
                 ['sthā', 'sthita', 'sthiti'],
                 ['vac', 'ukta', 'vāc'],
                 ['deś', 'deśita', 'deśanā'],
                 ['darś', 'dṛṣṭa', 'dṛṣṭi'],
                 ['kath', 'kathita', 'kathā'],
                 ['abhiniviś', 'abhiniviṣṭa', 'abhiniveśa'],
                 ['sev', 'sevita', 'sevā'],
                 ['vyavahar', 'vyavahṛta', 'vyavahāra'],
                 ['vikalp', 'vikalpita', 'vikalpa'],
                 ['saṃkalp', 'saṃkalpita', 'saṃkalpa'],
                 ['saṃjan', 'saṃjñita', 'saṃjñā'],
                 ['prajñap', 'prajñapta', 'prajñapti'],
                 ['spṛś', 'spṛṣṭa', 'sparśa'],
                 ['car', 'cārita', 'cāra'],
                 ['vicār', 'vicārita', 'vicāra'],
                 ['vart', 'vartita', 'vṛtti'],
                 ['avalok', 'avalokita', 'avaloka'],
                 ['vyavalok', 'vyavalokita', 'vyavaloka'],
                 ['labh', 'labdha', 'lābha'],
                 ['gṛhṇ', 'gṛhīta', 'graha'],
                 ['śṛṇ', 'śruta', 'śruti'],
                 ['kar', 'kṛta', 'kāra']]

    for c in columns:
        current_analogies = []
        for a in analogies:
            current_analogies.append([a[c[0]], a[c[1]]])
        print(current_analogies)
        f = open(args.log_file, 'a')
        f.write(str(column_names[c[0]]) + '-' + str(column_names[c[1]]))
        f.write("\n")
        at1, at5, at10, count = evaluate_all_pairs(args.model_path, current_analogies)
        f.write("@1: {} @5: {} @10:{}".format(str(at1), str(at5)
                                          , str(at10)))
        f.write("\n")
        f.close()
