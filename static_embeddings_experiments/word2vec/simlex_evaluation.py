from gensim.models import Word2Vec
import pandas as pd
import numpy as np
from scipy.stats import spearmanr

import argparse


def simlex_evaluation(simlex_file, model_path, log_file):
    data = pd.read_csv(simlex_file)
    model = Word2Vec.load(model_path)
    gold_similarities = data['scaled']

    similarities = []
    gold_similarities = []
    for index, row in data.iterrows():
        try:
            w1 = model.wv[row['word1']]
            w2 = model.wv[row['word2']]
            cos = np.dot(w1, w2) / (np.sqrt(np.dot(w1, w1)) * np.sqrt(np.dot(w2, w2)))
            similarities.append(cos)
            gold_similarities.append((row['ScoreA'] + row['ScoreB'] + row['ScoreC']) / 3)
        except:
            print("The following pair was not found: " + row['word1'] + " " + row['word2'])

    correlation = spearmanr(gold_similarities, similarities)
    with open(log_file, 'a') as f:
        f.write("Spearman correlation\n")
        f.write("Correlation: " + str(correlation[0]))
        f.write("p-value: " + str(correlation[1]))

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("--model_path",
                        type=str,
                        required=True)
    parser.add_argument("--evaluation_file",
                        type=str,
                        default="../../resources/AllPairsScored_v3_2021-10-20.csv")
    parser.add_argument("--log_file",
                        type=str,
                        default="./word2vec_simlex_log.txt")
    
    args = parser.parse_args()
    simlex_evaluation(args.evaluaiton_file, args.model_path, args.log_file)
