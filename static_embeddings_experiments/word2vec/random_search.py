import random
import numpy as np
import os
import argparse

from gensim.models import Word2Vec
from gensim.test.utils import datapath
from gensim.models.word2vec import LineSentence

from static_embeddings_experiments.word2vec.analogy_evaluation import evaluate


def random_search(data_file, model_path):
    random.seed(42)
    log = os.path.join(model_path, "log")
    parameters = {}
    
    #subset of the analogies dataset which was used for hyperparameter optimization
    analogies = [['abhilap', 'abhilāpa'],
                 ['kalp', 'kalpa'],
                 ['smar', 'smṛti'],
                 ['sthā', 'sthiti'],
                 ['vac', 'vāc'],
                 ['deś', 'deśanā'],
                 ['darś', 'dṛṣṭi'],
                 ['kath', 'kathā'],
                 ['abhiniviś', 'abhiniveśa'],
                 ['sev', 'sevā'],
                 ['vyavahar', 'vyavahāra'],
                 ['vikalp', 'vikalpa'],
                 ['saṃkalp', 'saṃkalpa'],
                 ['saṃjan', 'saṃjñā'],
                 ['prajñap', 'prajñapti'],
                 ['spṛś', 'sparśa'],
                 ['car', 'cāra'],
                 ['vicār', 'vicāra'],
                 ['vart', 'vṛtti'],
                 ['avalok', 'avaloka'],
                 ['vyavalok', 'vyavaloka'],
                 ['labh', 'lābha'],
                 ['gṛhṇ', 'graha'],
                 ['śru', 'śruti']]

    for i in range(100):
        model_name = os.path.join(model_path, "model_" + str(i))
        print("Training model " + str(i) + "...")
        sentences = LineSentence(datapath(data_file))
        parameters['sg'] = random.choice([0, 1])
        parameters['vector_size'] = random.choice([50, 100, 150, 200, 250, 300])
        parameters['window'] = random.choice(np.arange(5, 11))
        parameters['epochs'] = random.choice(np.arange(3, 16))
        with open(log, 'a') as f:
            f.write("Model {}\n".format(str(i)))
            for k, v in parameters.items():
                f.write("{}: {}\n".format(k, str(v)))
        model = Word2Vec(sentences=sentences, vector_size=parameters['vector_size'], 
                        window=parameters['window'], sg=parameters['sg'],
                        epochs=parameters['epochs'],
			            min_count=1)
        model.save(model_name)
        print("Evaluating model " + str(i) + "...")
        at1, at5, at10 = evaluate(model_name, analogies)
        with open(log, 'a') as f:
            f.write("@1: {}, @5: {}, @10: {}\n".format(at1, at5, at10))
            f.write("\n")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("--data_path",
                        type=str,
                        required=True)
    
    args = parser.parse_args()
    random_search(args.data_path, "../hyperparameter_optimization/word2vec")
