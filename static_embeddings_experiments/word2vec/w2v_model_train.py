from gensim.models import Word2Vec
from gensim.test.utils import datapath
from gensim.models.word2vec import LineSentence
from gensim.models import KeyedVectors

import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("--data_path",
                        type=str,
                        required=True)
    parser.add_argument("--model_path",
                        type=str,
                        default="./trained_w2vec_model")
    parser.add_argument("--vector_size",
                        type=int,
                        default=300)
    parser.add_argument("--window_size",
                        type=int,
                        default=11)
    parser.add_argument("--epochs",
                        type=int,
                        default=80)
    parser.add_argument("--model",
                        type=int,
                        default=0)

    args = parser.parse_args()

    sentences = LineSentence(datapath(args.data_path))
    model = Word2Vec(sentences, sg=args.model, vector_size=args.vector_size, min_count=1, window=args.window_size,
                    epochs=args.epochs)
    model.save(args.model_path)
