"""
This is the second version of corpus reconstruction script which reconstruct the sentences in the sanskrit corpus
so they can be consumed by the gensim library as well
"""
import pandas as pd
import os


def extract_lemmatized_sentences_from_tsv(file, remove_corrupted=True):
    """Extraxts the lemmatized sentences from the original files."""
    data = pd.read_csv(file)
    text = ''
    for index, row in data.iterrows():
        if str(row['word']) == '</s>':
            text += "\n"
        elif str(row['PoS']) == 'punct':
            continue
        elif pd.isna(row['WordID']):
            continue
        elif str(row['lemma']) == "CORRUPTED":
            if remove_corrupted:
                continue
            else:
                text += "***UNK***"
                text += " "
        else:
            text += str(row['lemma'])
            text += " "
    return text


def transform_from_csv_to_txt(csv_path, output_file):
    files = os.listdir(csv_path)
    text = ""
    for f in files:
        f = os.path.join(csv_path, f)
        t = extract_lemmatized_sentences_from_tsv(f)
        text += t

    with open(output_file, "w") as f:
        f.write(text)


if __name__ == "__main__":
    transform_from_csv_to_txt("../../resources/BuddhCorpus_v2c_IncludesReconstructions_2021-11-11",
                              "./BuddhCorpus_v2c_IncludesReconstructions_no_punct_no_corruptions_lemmatized.txt",
                              lemmatized=True)
