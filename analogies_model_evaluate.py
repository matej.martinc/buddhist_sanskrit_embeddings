import dill
import numpy as np
import editdistance
import unidecode
import argparse


def remove_diacritics(s):
    unaccented_string = unidecode.unidecode(s)
    return unaccented_string

def check_editdistance(target_word_list, compare_word, treshold=0.7):
    farEnough = True
    for w in target_word_list:
        ed = 1 - (float(editdistance.eval(w, compare_word)) / max(len(w), len(compare_word)))
        if ed > treshold:
            farEnough = False
            break
    return farEnough

def normalize(d_embeds):
    norm_embeds = {}
    for w, e in d_embeds.items():
        normalized_e = e / np.sqrt(np.sum(e ** 2))
        norm_embeds[w] = normalized_e
    return norm_embeds

def undiacritize_embeds(d_embeds):
    undiacritized_embeds = {}
    for w, e in d_embeds.items():
        undiacritized_embeds[unidecode.unidecode(w)] = e
    return undiacritized_embeds


def get_analogy(count, word1, word2, word3, all_words, emb, w2idx, K=30):
    #print(f"{count}.) Nearest neighbors of analogy {word1} vs {word2} = {word3} vs ?")

    word_emb = (-w2idx[word1] + w2idx[word2] + w2idx[word3]).tolist()

    scores = (emb / np.linalg.norm(emb, 2, 1)[:, None]).dot(word_emb / np.linalg.norm(word_emb))
    k_best = scores.argsort()[-K:][::-1]
    best = []
    for i, idx in enumerate(k_best):
        close = all_words[idx]

        if close != word1 and close != word2 and close != word3 \
                and close not in best and close.isalnum() and len(best) < K and not any(char.isdigit() for char in close):
            best.append((close, str(round(scores[idx], 2))))
    #print(";".join([str(x[0]) + ':' + str(x[1]) for x in best[:10]]))
    #print('------------------------------------------------------------')

    return best


def load_bert_vec(emb_path, lang):
    word2vec = {}
    bert_embeddings, _ = dill.load(open(emb_path, 'rb'))
    for i, word in enumerate(bert_embeddings.keys()):
        vect = bert_embeddings[word][lang][0][0]/bert_embeddings[word][lang][0][1]
        if word in word2vec:
            print(word, 'found twice')
            continue
        word2vec[word] = vect
    return word2vec

def evaluate(model_path, analogies, columns):
    model = load_bert_vec(model_path, 'buddh')
    missing_words = []
    at1 = 0
    at5 = 0
    at10 = 0
    count = 0
    all_words = [x[0] for x in model.items()]
    embeds = np.array([x[1] for x in model.items()])
    for index, a in enumerate(analogies):
        for i in range(0, len(analogies)):
            if index != i:
                try:
                    predictions = get_analogy(count, a[columns[0]], a[columns[1]], analogies[i][columns[0]], all_words, embeds, model)
                    predictions = predictions[:10]
                    pred_at_1 = predictions[0][0]
                    truth = analogies[i][columns[1]]
                    if pred_at_1 == truth:
                        at1 += 1

                    pred = predictions[:5]
                    pred_at_5 = []
                    for p in pred:
                        pred_at_5.append(p[0])
                    if truth in pred_at_5:
                        at5 += 1

                    pred_at_10 = []
                    for p in predictions:
                        pred_at_10.append(p[0])
                    if truth in pred_at_10:
                        at10 += 1
                    count += 1
                except Exception as e:
                    missing_words.append(str(e).replace('"', '').replace("'", ''))
    at1 = at1 / count
    at5 = at5 / count
    at10 = at10 / count
    return at1, at5, at10, set(missing_words)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--embeddings_path",
                        default='embeddings/embeddings.pickle', type=str,
                        help="Path to pickle file containing embeddings.")
    args = parser.parse_args()

    column_configs = [[0,2],[0,1],[1,2]]
    column_map = ['verb', 'ppp', 'noun']
    analogies = [['kalp', 'kalpita', 'kalpa'],
                 ['smar', 'smṛta', 'smṛti'],
                 ['sthā', 'sthita', 'sthiti'],
                 ['vac', 'ukta', 'vāc'],
                 ['deś', 'deśita', 'deśanā'],
                 ['darś', 'dṛṣṭa', 'dṛṣṭi'],
                 ['kath', 'kathita', 'kathā'],
                 ['abhiniviś', 'abhiniviṣṭa', 'abhiniveśa'],
                 ['sev', 'sevita', 'sevā'],
                 ['vyavahar', 'vyavahṛta', 'vyavahāra'],
                 ['vikalp', 'vikalpita', 'vikalpa'],
                 ['saṃkalp', 'saṃkalpita', 'saṃkalpa'],
                 ['saṃjan', 'saṃjñita', 'saṃjñā'],
                 ['prajñap', 'prajñapta', 'prajñapti'],
                 ['spṛś', 'spṛṣṭa', 'sparśa'],
                 ['car', 'cārita', 'cāra'],
                 ['vicār', 'vicārita', 'vicāra'],
                 ['vart', 'vartita', 'vṛtti'],
                 ['avalok', 'avalokita', 'avaloka'],
                 ['vyavalok', 'vyavalokita', 'vyavaloka'],
                 ['labh', 'labdha', 'lābha'],
                 ['gṛhṇ', 'gṛhīta', 'graha'],
                 ['śṛṇ', 'śruta', 'śruti'],
                 ['kar', 'kṛta', 'kāra']]

    for cc in column_configs:
        task = column_map[cc[0]] + '-' + column_map[cc[1]]
        at1, at5, at10, missing_words = evaluate(args.embeddings_path, analogies, cc)
        missing_words = sorted(list(missing_words))
        print(f'Analogy columns {task} :acc@1 {at1:.4f}; acc@5: {at5:.4f}; acc@10 {at10:.4f};')
        if missing_words:
            print('Words not found in the vocabulary: ', missing_words)
        print('----------------------------')
