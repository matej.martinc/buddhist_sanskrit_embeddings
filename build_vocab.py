from collections import defaultdict
import pandas as pd
import os
import dill
from transformers import PreTrainedTokenizerFast
import argparse


class Vocab():
    def __init__(self, w_tokenizer):
        self.docs = defaultdict(list)
        self.target_docs = defaultdict(list)
        self.chunks = []
        self.meta = defaultdict(list)
        self.w_tokenizer = w_tokenizer

    def add(self, doc, target_doc, chunk, source):
        if chunk not in self.chunks:
            self.chunks.append(chunk)
        self.docs[chunk].append(doc)
        self.target_docs[chunk].append(target_doc)
        self.meta[chunk].append(source)

    def make_vocab(self, vocab_path):
        print('making_vocab')
        all_freqs = []
        freqs = defaultdict(int)
        punctuation = ['"', '*', '+', '<', '=', '>', '@', '[', '\\', ']', '^', '`', '{', '|', '}', '~', '//', '/']
        for chunk in self.chunks:
            chunk_freqs = defaultdict(int)
            count_words = 0
            for doc in self.target_docs[chunk]:
                for sent in doc.split(" <eos> "):
                    for word in sent.split():
                        is_punct = False
                        for p in punctuation:
                            if p in word:
                                is_punct = True
                                break
                        if not is_punct:
                            is_digit = word.isdigit()
                            if not is_digit:
                                chunk_freqs[word] += 1
                                freqs[word] += 1
                                count_words += 1
            all_freqs.append((chunk_freqs, count_words))
        print('All vocab size: ', len(freqs))

        filtered_freqs = []
        for word, freq in freqs.items():
            allow = True
            for chunk_freq, _ in all_freqs:
                if chunk_freq[word] < 5:
                    allow = False
                    break

            if allow:
                filtered_freqs.append((word, freq))

        print('Len filtered vocab: ', len(filtered_freqs))
        self.freqs = []
        freqs = sorted(filtered_freqs, key=lambda x: x[1], reverse=True)
        with open(vocab_path, 'w', encoding='utf8') as f:
            f.write('word,frequency\n')
            for w, freq in freqs:
                w = self.w_tokenizer.tokenize(w)
                w = "".join(w).replace('##', '')
                f.write(w + ',' + str(freq) + '\n')
                self.freqs.append((w, freq))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--data_path",
                        default='resources/corpus_preprocessed.tsv',
                        type=str,
                        help="Paths to the preprocessed buddhist corpus")
    parser.add_argument("--output_dir",
                        default='resources',
                        type=str,
                        help="Path to the folder that will contain generated output vocab")
    parser.add_argument("--tokenizer",
                        default='tokenizers/sanskrit.json',
                        type=str,
                        help="Path to the trained tokenizer")

    args = parser.parse_args()

    if not os.path.exists(args.output_dir):
        os.makedirs(args.output_dir)

    vocab_path = os.path.join(args.output_dir, 'vocab.csv')
    vocab_pickle_path = os.path.join(args.output_dir, 'vocab.pickle')
    w_tokenizer = PreTrainedTokenizerFast(tokenizer_file=args.tokenizer)
    df_data = pd.read_csv(args.data_path, sep='\t', encoding='utf8')

    all_sents = []
    vocab = Vocab(w_tokenizer)
    all_data = []
    all_sents = []
    all_sources = []
    source_counts = defaultdict(int)
    for idx, row in df_data.iterrows():
        chunk = 'buddh'
        meta = [row['id'], row['source']]
        source_counts[chunk] += 1
        text = row['text']
        target_text = row['lemmatized_text']
        vocab.add(text, target_text, chunk, meta)
        sents = text.split(' <eos> ')
        all_sents.extend(sents)
        all_sources.append(chunk)
    print('Sources in vocab: ', list(set(all_sources)))

    vocab.make_vocab(vocab_path)
    with open(vocab_pickle_path, 'wb') as handle:
        dill.dump(vocab, handle)

    print('Done building vocab.')





