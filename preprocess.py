import pandas as pd
import argparse
import random
import os

def preprocess_buddh(input_path, output_dir):
    all_data = []
    files = os.listdir(input_path)
    counter = 0
    for f in files:
        print('Processing', f)
        doc_sents = []
        counter += 1
        entire_path = os.path.join(input_path, f)
        try:
            df = pd.read_csv(entire_path, encoding='utf8', sep=',')
        except:
            print("Can't process file", f)
            continue
        sent = []
        lemma_sent = []
        fill = False
        for idx, row in df.iterrows():
            if str(row['word']).startswith('<s'):
                if len(sent) > 0:
                    print('Something is wrong', sent)
                    sent = []
                    lemma_sent = []
                fill = True
            elif str(row['word']).startswith('</s') or str(row['word']).startswith('<\\s>'):
                fill = False
                doc_sents.append((" ".join(sent), " ".join(lemma_sent)))
                sent = []
                lemma_sent = []
            elif fill:
                word = str(row['word']).replace('.', '').replace('?', '').replace('!', '')
                lemma = str(row['lemma']).replace('.', '').replace('?', '').replace('!', '')
                if lemma != 'CORRUPTED':
                    sent.append(word)
                    if lemma == 'nan':
                        lemma_sent.append(word)
                    else:
                        lemma_sent.append(lemma.replace('_', '-'))
        doc = [x[0] for x in doc_sents]
        lemma_doc = [x[1] for x in doc_sents]
        all_data.append((counter, f, " <eos> ".join(doc), " <eos> ".join(lemma_doc)))
        print(counter, f, " <eos> ".join(lemma_doc)[-100:])

    df = pd.DataFrame(all_data, columns=['id', 'source', 'text', 'lemmatized_text'])
    output_path = os.path.join(output_dir, 'corpus_preprocessed.tsv')
    df.to_csv(output_path, sep='\t', encoding='utf8', index=False)
    print('Preprocessed dataset written to:', output_path)
    return df



if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--data_path", default='resources/BuddhCorpus_v2c_IncludesReconstructions_2021-11-11', type=str, help='Path to Buddhist corpus')
    parser.add_argument("--output_folder", default='resources', help="Path to folder that contains output language model train and test sets")
    args = parser.parse_args()

    df = preprocess_buddh(args.data_path, args.output_folder)

    data = df['text'].tolist()

    random.shuffle(data)
    print('Num docs:', len(data))
    valid_index = int(0.9 * len(data))

    output_train = open(os.path.join(args.output_folder,'lm_train.txt'), 'w', encoding='utf8')
    output_test = open(os.path.join(args.output_folder, 'lm_test.txt'), 'w', encoding='utf8')

    for idx, doc in enumerate(data):
        doc = doc.replace(' <eos> ', ' ')
        if len(doc.strip()) > 0:
            if idx < valid_index:
                output_train.write(doc + '\n')
            else:
                output_test.write(doc + '\n')

    output_train.close()
    output_test.close()
    print('Done, language model train and test sets written to folder', args.output_folder)

