import pandas as pd
import numpy as np
import dill
import argparse
from scipy.stats import spearmanr

def normalize(d_embeds):
    norm_embeds = {}
    for w, e in d_embeds.items():
        normalized_e = e / np.sqrt(np.sum(e ** 2))
        norm_embeds[w] = normalized_e
    return norm_embeds

def load_bert_vec(emb_path, lang):
    word2vec = {}
    bert_embeddings, _ = dill.load(open(emb_path, 'rb'))
    for i, word in enumerate(bert_embeddings.keys()):
        vect = bert_embeddings[word][lang][0][0]/bert_embeddings[word][lang][0][1]
        if word in word2vec:
            print(word, 'found twice')
            continue
        word2vec[word] = vect
    return word2vec

def simlex_evaluation(simlex_file, model_path):
    data = pd.read_csv(simlex_file)
    model = load_bert_vec(model_path, 'buddh')
    data['gs'] = (data['ScoreA'] + data['ScoreB'] + data['ScoreC']) / 3
    gold_similarities = data['gs']

    similarities = []
    for index, row in data.iterrows():
        try:
            w1 = model[row['word1']]
            w2 = model[row['word2']]
            cos = np.dot(w1, w2) / (np.sqrt(np.dot(w1, w1)) * np.sqrt(np.dot(w2, w2)))
        except:
            print("Missing word: ", row['word1'])
        similarities.append(cos)

    correlation = spearmanr(gold_similarities, similarities)
    return correlation

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--embeddings_path",
                        default='embeddings/embeddings.pickle', type=str,
                        help="Path to pickle file containing embeddings.")
    parser.add_argument("--simlex_path",
                        default='resources/AllPairsScored_v3_2021-10-20.csv', type=str,
                        help="Path to file containing simlex word pairs.")
    args = parser.parse_args()

    correlation = simlex_evaluation(args.simlex_path, args.embeddings_path)
    print(f'Spearman correlation: {correlation[0]:.4f}; P-value: {correlation[1]:.7f}')
