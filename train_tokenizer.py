import argparse
from tokenizers.implementations import BertWordPieceTokenizer
from tokenizers.pre_tokenizers import Whitespace

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--files",
        default='resources/train_reference_corpus_processed.txt,resources/test_reference_corpus_processed.txt,resources/lm_train.txt,resources/lm_test.txt',
        metavar="path",
        type=str,
        help="The files to use as training; accept '**/*.txt' type of patterns \
                              if enclosed in quotes",
    )
    parser.add_argument(
        "--out",
        default="tokenizers/sanskrit.json",
        type=str,
        help="Path to the output directory, where the files will be saved",
    )

    args = parser.parse_args()

    # Initialize an empty tokenizer
    tokenizer = BertWordPieceTokenizer(strip_accents=False)
    tokenizer.pre_tokenizer = Whitespace()

    tokenizer.train(files=args.files.split(','), limit_alphabet=1000,
                    wordpieces_prefix='##',
                    special_tokens=['[PAD]', '[UNK]', '[CLS]', '[SEP]', '[MASK]'])


    tokenizer.save(args.out)
    print('Tokenizer saved in', args.out)